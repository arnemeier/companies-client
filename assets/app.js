// Initialization after the HTML document has been loaded...
window.addEventListener('DOMContentLoaded', () => {

    // Function calls for initial page loading activities...
    doLoadCompanies();
});

/* 
    --------------------------------------------
    ACTION FUNCTIONS
    --------------------------------------------
*/

async function doLoadCompanies() {
    console.log('Loading companies...');
    let companies = await fetchCompanies();
    displayCompanies(companies);
}

async function doDeleteCompany(companyId, companyName) {
    console.log(companyId);
    if (confirm("Kas soovid kusutada ettevõtet " + companyName + "?")) {
        await removeCompany(companyId);
        await doLoadCompanies(); //await juhuks kui tuleks peale seda veel miskit ette võtta
    }
}

/* 
    --------------------------------------------
    DISPLAY FUNCTIONS
    --------------------------------------------
*/

function displayCompanies(companies) {
    const mainElement = document.querySelector('main');
    let companiesHtml = '';
    companies.forEach(element => {
        companiesHtml += `
        <div class="container">
            <div class="company-box">
                Ettevõtte nimi: ${element.name}
                <br>
                Asutamise aeg: ${element.established}
                <br>
                Töötajate arv: ${element.employees}
                <br>
            </div>
            <div class="logo-box">
                <img src="${element.logo}">
            </div>
            <div class="control-box">
                <button class="button-red" onclick="doDeleteCompany(${element.id}, '${element.name}')";>Kustuta</button>
            </div>
        </div>          
        `;
    });
    mainElement.innerHTML = companiesHtml;
}

async function displayCompanyEditPopup(id) {
    await openPopup(POPUP_CONF_DEFAULT, 'companyEditFormTemplate');
}
