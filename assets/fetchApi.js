
async function fetchCompanies() {

    try {
        const response = await fetch(`${API_URL}/companies/all`);
        return await response.json();
    } catch (error) {
        console.log('ERROR!', error);
    }

}

async function removeCompany(companyId){
    try {
        const response = await fetch(`${API_URL}/companies/${companyId}`, {method: 'DELETE'});
        return await response.json();

    } catch (error) {
        console.log('ERROR!', error);

    }


}
